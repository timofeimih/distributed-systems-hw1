
import sys

def start(file):
	file1 = open(file, 'r')
	Lines = file1.readlines()


	nextIsKeySpace = nextIsNodes = nextIsShortcuts= 0 

	minKeySpace = maxKeySpace = 0
	nodesObject = {}
	nodes = []
	count = 0
	# Strips the newline character
	for line in Lines:
	    count += 1

	    if(nextIsKeySpace):
	    	minKeySpace = int(line.split(", ")[0])
	    	maxKeySpace = int(line.split(", ")[1])
	    	nextIsKeySpace = 0
	    elif(nextIsNodes):
	    	nodes = line.strip().split(", ")
	    	nodes.sort(key=int)
	    	nodesInt = []
	    	for node in nodes:
	    		node = int(node)
	    		if(node > maxKeySpace):
	    			continue
	    		if(node < minKeySpace):
	    			continue
	    		nodesInt.append(node)

	    	nodes = nodesInt
	    	enumeratedNodes = enumerate(nodes)
	    	for i, node in enumeratedNodes:
	    	
	    		successor = nextSuccessor = 0
	    		if(len(nodes) > i+1):
	    			successor = nodes[i+1]
	    			successorNumber = i+1
	    		else:
	    			successor = nodes[0]
	    			successorNumber = 0

	    		if(len(nodes) > successorNumber + 1):
	    			nextSuccessor = nodes[successorNumber+1]
	    		else:
	    			nextSuccessor = nodes[0]

	    		nodesObject[node] = [[], successor, nextSuccessor]
	    	nextIsNodes = 0
	    elif(nextIsShortcuts):
	    	shortcuts = line.strip().split(", ")
	    	for shortcut in shortcuts:
	    		item = shortcut.split(':')
	    		nodesObject[int(item[0])][0].append(int(item[1]))
	    	nextIsShortcuts = 0

	    if(line.strip() == "#key-space"):
	    	nextIsKeySpace = 1
	    elif(line.strip() == "#nodes"):
	    	nextIsNodes = 1
	    elif(line.strip() == "#shortcuts"):
	    	nextIsShortcuts = 1


	def rebuildObjectLinks(nodesObject, nodes, checkShortcuts = False):

		nodes.sort()
		enumeratedNodes = enumerate(nodes)
		for i, node in enumeratedNodes:
			successor = 0
			nextSuccessor = 0
			if(len(nodes) > i+1):
				successor = nodes[i+1]
				successorNumber = i+1
			else:
				successor = nodes[0]
				successorNumber = 0

			if(len(nodes) > successorNumber + 1):
				nextSuccessor = nodes[successorNumber+1]
			else:
				nextSuccessor = nodes[0]

			shortcuts = nodesObject[node][0]
			if(checkShortcuts):
				for p in set(shortcuts) - set(nodes):
					shortcuts.remove(p)

			nodesObject[node] = [shortcuts, successor, nextSuccessor]

		nodesObjectSorted = sorted(nodesObject.items(), key=lambda x: int(x[0]))
		print("REBUILDED", nodesObjectSorted)

		return nodesObject

	waitCommand = 1
	while(waitCommand):
		command = input('Enter command(Exit command to quit):')
		#command = "Remove 17, 22, 56"
		#print(nodesObject)
		#print("COMMAND START", command)
		if(command == "List"):
			for key in nodesObject:
				print(str(key) + ":" + ",".join(str(x) for x in nodesObject[key][0]) + ", S-" + str(nodesObject[key][1]) + ", NS-" + str(nodesObject[key][2]))
		elif("Lookup" in command):
			command = command.split(' ')[1]
			steps = 0
			search = 1
			if(":" in command):
				node = int(command.split(":")[1])
				endNode = int(command.split(":")[0])
				finalNode = 0

				while search:
					steps = steps + 1
					if(len(nodesObject[node][0])):
						for shortcut in nodesObject[node][0][::-1]:
							if(shortcut < endNode):
								node = shortcut
								break;

					if(nodesObject[node][2] < endNode):
						node = nodesObject[node][2]
						continue;
					elif(nodesObject[node][1] < endNode):
						node = nodesObject[node][1]
						continue;
					else:
						steps = steps + 1
						finalNode = nodesObject[node][1]
						search = 0

			else:
				node = nodes[0]
				endNode = int(command)
				finalNode = 0
				print(node)

				while search:
					steps = steps + 1
					if(len(nodesObject[node][0])):
						for shortcut in nodesObject[node][0][::-1]:
							if(shortcut < endNode):
								node = shortcut
								break;

					if(nodesObject[node][2] < endNode):
						node = nodesObject[node][2]
						continue;
					elif(nodesObject[node][1] < endNode):
						node = nodesObject[node][1]
						continue;
					else:
						finalNode = nodesObject[node][1]
						search = 0

			print("Data stored in node ", finalNode, "-", steps,"request sent")
		elif("Join" in command):
			nodeToJoin = int(command.split(" ")[1])
			index = 0

			correctNode = 1
			if(nodeToJoin > maxKeySpace):
				correctNode = 0
			if(nodeToJoin < minKeySpace):
				correctNode = 0

			if(correctNode):
				nodes.append(nodeToJoin)
				nodes.sort()

				nodesObject[nodeToJoin] = [[], 0, 0]
				nodesObject = rebuildObjectLinks(nodesObject, nodes)

		elif("Leave" in command):
			if(len(nodes) > 1):
				node = int(command.split(" ")[1])
				enumeratedNodes = enumerate(nodes)
				for i, nodeItem in enumeratedNodes:
					if(nodeItem == node):
						nodes.pop(i)
						break;
				nodesObject.pop(node, '')
				nodesObject = rebuildObjectLinks(nodesObject, nodes)
			else:
				print("Node cannot leave, because there is less than one node")
		elif("Shortcut" in command):
			shortcut = command.split(" ")[1].split(":")
			nodesObject[int(shortcut[0])][0].append(int(shortcut[1]))
		elif("Remove" in command):
			nodesToRemove = [int(node) for node in command.split("Remove ")[1].split(", ")]

			for key in nodesToRemove:
				nodesObject.pop(key, '')
			
			nodes = list(set(nodes) - set(nodesToRemove))
			nodes.sort()

			nodesObject = rebuildObjectLinks(nodesObject, nodes, True)
		elif("Exit" == command):
			print("Program exit")
			waitCommand = 0
#10) To handle resilience when more that one node leave I did implement the index variable to store all node indexes. So we remove from object containing all node info and index variable nodes that we should delete and after that we should rebuild our object with data, because we need to apply new next successor and successor parameters and also remove some shortcuts if there some. We could also improve this program by adding some close shortcut instead of ones that been removed. But I do not know, do we need that for our program or not.


def usage():
    print("Usage:")
    print("filename")
    sys.exit(1)


if len(sys.argv) != 2:
    usage()
else:
    start(sys.argv[1])